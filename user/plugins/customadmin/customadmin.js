l$ = {
    footer: false,
    donate: false
}

document.getElementsByTagName("html")[0].setAttribute("lang", "fr");

var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://framasoft.org/nav/nav.js";
    document.getElementsByTagName('head')[0].appendChild(script);

jQuery(document).ready(function(){

    // Grav installé en sous-dossier pour la personnalisation en local
    var gravRoot = (location.href.indexOf('/grav/') > -1) ? '/grav' : '';

    /* Message d’avertissement */
    jQuery('#messages').append('<div class="info alert">Framasite est en phase de test. Le service fonctionne, mais n’est pas encore facile à utiliser par quiconque. C’est à l’écoute de vos retours que nous allons l’améliorer et le documenter au cours des semaines à venir.</div>');
    /* C’est masqué mais c’est aussi désactivé en dur */
    var hiddenFormSections = [
        // Système
        '[name="data[pages][expires]"]',      // En-tête HTTP
        '[id="toggle_cache.enabled1"]',       // Cache
        '[id="toggle_twig.cache1"]',          // Twig
        '[id="toggle_assets.css_pipeline1"]', // Asset
        '[name="data[errors][display]"]',     // Erreurs
        '[id="toggle_debugger.enabled1"]',    // Debug
        '[name="data[session][timeout]"]',    // Session
        '[id="toggle_gpm.releasesstable"]',   // Avancé
    ];
    jQuery(hiddenFormSections.join()).parent().parent().parent().parent().parent().hide();

    var hiddenSettings = [
        '[data-grav-field-name="data[pages][process]"]', // Traitement Md/Twig
        '[data-grav-field-name="data[pages][events]"]',  // Événements Page/Twig
        '[name="data[pages][append_url_extension]"]',    // Ajouter l’extension .html (conflit de template)
        '[data-grav-field-name="data[pages][redirect_default_route]"]',  // Route redirection
        '[name="data[pages][redirect_default_code]"]',
        '[data-grav-field-name="data[pages][redirect_trailing_slash]"]',
        '[data-grav-field-name="data[pages][ignore_hidden]"]',
        '[name="data[pages][ignore_files]"]',
        '[name="data[pages][ignore_folders]"]',
        '[data-grav-field-name="data[pages][twig_first]"]',
        '[data-grav-field-name="data[pages][never_cache_twig]"]',
        '[data-grav-field-name="data[pages][frontmatter][process_twig]"]',
        '[name="data[pages][frontmatter][ignore_fields]"]',
        '[data-grav-field-name="data[languages][override_locale]"]',
        '[name="data[images][cache_perms]"]',
        '[name="data[media][unsupported_inline_types]"]',
        '[name="data[media][allowed_fallback_types]"]',
        '[name="data[pages][theme]"]',

        // Options avancées des pages
        '[data-grav-field-name="data[header][process]"]',
        '[name="data[header][append_url_extension]"]',
        '[data-grav-field-name="data[header][twig_first]"]',
        '[data-grav-field-name="data[header][never_cache_twig]"]',
        '[data-grav-field-name="data[header][debugger]"]',
        '[name="data[header][template]"]',
        '[name="data[header][append_url_extension]"]'
    ];
    if(location.href.indexOf('/admin/pages/common')) {
        hiddenSettings.push('[name="data[header][visible]"]');
    }
    jQuery(hiddenSettings.join()).parent('.form-field').hide();
    jQuery(hiddenSettings.join()).parent().parent().parent('.form-field').hide();

    jQuery('[id="tab-content.options.advanced2"] h1:contains("JSComments")').hide();
    jQuery('[id="tab-content.options.advanced2"] h1:contains("JSComments")').next('.form-section').hide();

    var hiddenLinks = [
        '#admin-menu [href="'+gravRoot+'/admin/themes"]',        // Themes (seulement Gravstrap et pas de paramétrage derrière)
        '#admin-menu [href="'+gravRoot+'/admin/tools"]',         // Tools (import/export de zip)
        '.form-tabs [href="'+gravRoot+'/admin/config/media"]',   // onglet Medias
        '.form-tabs [href="'+gravRoot+'/admin/config/info"]',    // onglet Info
        '#titlebar [data-gpm-checkupdates]',                     // Vérifier les mises à jour
        '#titlebar [href="'+gravRoot+'/admin/themes/install"]',  // Ajouter thèmes
        '#titlebar [href="'+gravRoot+'/admin/plugins/install"]', // Ajouter plugins
        '#admin-dashboard #notifications',                       // Notifications (de Grav, en anglais)
        '#admin-dashboard #news-feed',                           // Fil d’actualité (de Grav, en anglais)
        '.gpm-plugins .danger',                                  // Supprimer un plugin
        '#updates .updates-chart',                               // Bloc Maintenance mises à jour
        '[data-delete-url^="'+gravRoot+'/admin/pages/common/task:delete"]',  // Boutons de suppression des dossiers common et images
        '[data-delete-url^="'+gravRoot+'/admin/pages/images/task:delete"]'
    ]
    jQuery(hiddenLinks.join()).hide();

    jQuery('.notice.alert [href="https://github.com/getgrav/grav-plugin-admin/issues"]').parent('.notice.alert').hide();
    jQuery('#grav-update-button').parent('.alert.grav').hide();

    var lockedPlugins = [
        '[href^="'+gravRoot+'/admin/plugins/blog-injector/task"]',  // BlogInjector
        '[data-gpm-plugin="blog-injector"] ~ #blueprints',          // BlogInjector config
        '[href^="'+gravRoot+'/admin/plugins/gravstrap/task"]',      // Gravstrap
        '[data-gpm-plugin="gravstrap"] ~ #blueprints',              // Gravstrap config
        '[href^="'+gravRoot+'/admin/plugins/customadmin/task"]',    // Customadmin
    ]
    jQuery(lockedPlugins.join()).hide();


    /** IntroJS **/
    $('head').append(
        '<script src="'+location.protocol+'//'+location.host+gravRoot+'/user/plugins/customadmin/intro.min.js"></script>'+
        '<link href="'+location.protocol+'//'+location.host+gravRoot+'/user/plugins/customadmin/introjs.min.css" rel="stylesheet">'
    );

    // Tableau de bord
    if(location.href.indexOf('/admin/') == -1 ) {
        startHelp = function(){
            introJs()
                .setOptions({
                    'prevLabel': '← Précédent',
                    'nextLabel': 'Suivant →',
                    'skipLabel': '&times;',
                    'doneLabel': '&times;',
                    'scrollTo': 'tooltip',
                    'tooltipClass':'introjs-lg',
                    'showBullets':false,
                    steps: [
                        {
                            intro:
                                '<h5>Découvrez l’espace d’administration de votre Framasite</h5>'+
                                '<p>Voici un guide rapide pour vous présenter l’interface. N’hésitez pas à vous référer à la <a href="https://docs.framasoft.org/fr/grav/">documentation</a> pour une utilisation avancée.</p>'+
                                '<p>Lorsqu’une aide contextuelle est disponible comme ici, un bouton <a class="button" style="background-color:#00A6CF"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a> apparaît dans la barre d’entête.</p>'+
                                '<p>Cliquez sur les boutons « Suivant » et « Précédent » ci-dessus pour naviguer dans l’aide contextuelle.',
                        },
                        {
                            element: '#admin-menu [href$="config/system"]',
                            intro:
                                'Normalement le site est déjà correctement configuré mais si vous voulez changer le titre, l’auteur et la description. Ça se passe dans le menu « Configuration > Site ».',
                            position: 'right'
                        },
                        {
                            element: '#admin-menu [href$="pages"]',
                            intro:
                                'Des pages d’exemple vous sont proposées avec votre Framasite pour pouvoir rapidement créer un site fonctionnel. Pour les modifier ou en ajouter des nouvelles, ça se passe dans le menu « Pages »…',
                            position: 'right'
                        },
                        {
                            element: '#latest .button',
                            intro:
                                '… ou en suivant le bouton « Gestion des pages » ci-contre',
                            position: 'left'
                        },
                        {
                            element: '#admin-menu [href$="plugins"]',
                            intro:
                                'Le site est fourni avec plusieurs plugins (dé)sactivables selon vos besoins ainsi qu’un unique thème que vous pouvez personnaliser avec des feuilles de style grâce au plugin CustomCSS.',
                            position: 'right'
                        },
                        {
                            element: '#admin-menu [href$="backup-manager"]',
                            intro:
                                'Par la suite, si vous voulez exporter votre site Grav pour l’héberger ailleurs (les fonctionnalités étant volontairement limitées ici) ou simplement pour en conserver une copie, vous pouvez gérer vos sauvegardes depuis le menu « Backup ».',
                            position: 'right'
                        }
                    ]
                })
                .onbeforechange(function(targetElement){
                    $(targetElement).focus();
                })
                .onexit(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1);
                })
                .start();

            $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
        }

        $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>');

        // Affichage auto de l’aide contextuelle une seule fois
        if( !/(?:; )?introjs=([^;]*);?/.test(document.cookie) && $('#admin-main').length) {
            startHelp();
            var expires = new Date();
            expires.setTime(new Date().getTime() + 3153600000000);
            document.cookie = "introjs=true;expires=" + expires.toGMTString();
        }

    }

    // Configuration Site
    if(location.href.indexOf('/config/site') > -1) {
        startHelp = function(){
            introJs()
                .setOptions({
                    'prevLabel': '← Précédent',
                    'nextLabel': 'Suivant →',
                    'skipLabel': '&times;',
                    'doneLabel': '&times;',
                    'scrollTo': 'tooltip',
                    'highlightClass':'hidden',
                    'tooltipClass':'introjs-lg',
                    'showBullets':false,
                    steps: [
                        {
                            element: '[name="data[title]"]',
                            intro:
                                'Vous pouvez remplacer le titre de votre site. C’est ce titre qui apparait dans les résultats des moteurs de recherche et dans la fenêtre de votre navigateur web.',
                            position: 'left'
                        },
                        {
                            element: '[name="data[author][name]"]',
                            intro:
                                'Vous pouvez changer le nom de l’auteur…',
                            position: 'left'
                        },
                        {
                            element: '[name="data[author][email]"]',
                            intro:
                                '… et l’adresse email correspondante…',
                            position: 'left'
                        },
                        {
                            element: '[name="data[metadata][description]"]',
                            intro:
                                '… ainsi que la description par défaut de votre site. Elle ne sera pas visible sur votre site. Seuls les moteurs de recherche en tiennent compte. Vous pourrez ajouter d’autres métadonnées ici plus tard.',
                            position: 'left'
                        },
                        {
                            element: '#titlebar button[type="submit"]',
                            intro:
                                'N’oubliez pas d’enregistrer vos changements.',
                            position: 'left'
                        }
                    ]
                })
                .onbeforechange(function(targetElement){
                    $(targetElement).focus();
                })
                .onexit(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1);
                })
                .start();

            $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
        }

        $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>')

    }

    // Gestion des pages
    if(location.href == location.protocol+'//'+location.host+gravRoot+'/admin/pages') {
        startHelp = function(){
            introJs()
                .setOptions({
                    'prevLabel': '← Précédent',
                    'nextLabel': 'Suivant →',
                    'skipLabel': '&times;',
                    'doneLabel': '&times;',
                    'scrollTo': 'tooltip',
                    'tooltipClass':'introjs-lg',
                    'showBullets':false,
                    steps: [
                        {
                            intro:
                                'Pour vous aider dans la construction de votre site nous l’avons prérempli avec quelques pages d’exemples.<br>'+
                                'Une icône « maison » <i class="fa fa-home" style="color:#8d959a"></i> indique quelle est la page utilisée comme page d’accueil. Vous pouvez en changer dans la partie <a href="./config/system">Configuration</a>.<br>'+
                                'Les pages constituées de sous-pages (comme le blog) ou de sous-modules (comme la page carrousel) sont précédées de cette icône <i class="page-icon fa fa-plus-circle" style="color:#0082BA"></i>.',
                            position: 'bottom-middle-aligned'
                        },
                        {
                            element: '[data-nav-id="/contact"]',
                            intro:
                                '<img class="col-sm-6" src="../user/plugins/customadmin/images/th_contact.jpg" alt="" />'+
                                '<p class="col-sm-6">Exemple de formulaire de contact. Pour qu’il fonctione, il faut définir les adresses emails de l’expéditeur et du destinataire dans le plugin « email ».<br>Pour modifier le texte du formulaire et notamment le captcha, il faut passer en mode d’édition expert.</p>'+
                                '<p class="col-sm-12"><a href="../contact" class="btn btn-xs btn-primary">Voir</a> '+
                                '<a href="../admin/plugins/email" class="btn btn-xs btn-warning">Configurer</a> <a href="./pages/contact/mode:expert" class="btn btn-xs btn-info">Modifier (mode expert)</a></p>',
                            position: 'top'
                        },
                        {
                            element: '[data-nav-id="/common"]',
                            intro:
                                'C’est dans ce fichier que vous pouvez modifier la barre de navigation et le pied de page du site.'+
                                '<p class="col-sm-12"><a href="./pages/common/mode:normal" class="btn btn-xs btn-info">Modifier</a></p>',
                            position: 'top'
                        },
                        {
                            element: '[data-nav-id="/images"]',
                            intro:
                                'Ce dossier contient l’ensemble des images communes à tout le site. Il sert notament de référence pour le module <code>images-collage</code>.',
                            position: 'top'
                        },
                        {
                            element: '#titlebar .button-group button[data-remodal-target="modal"]',
                            intro:
                                'Cliquez sur ce bouton pour ajouter une nouvelle page ou un nouveau dossier. Remplissez ensuite le formulaire en précisant bien un « Template de page » correspondant à l’exemple qui vous convient parmi ceux proposés ici en démo.<br>Pour ajouter un article dans la section blog, il faut choisir le template <code>item</code> et <code>blog</code> comme « Page parente » (ou tout autre page utilisant le template <code>blog</code>).',
                            position: 'left'
                        }
                    ]
                })
                .onbeforechange(function(targetElement){
                    $(targetElement).focus();
                })
                .onexit(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1);
                })
                .start();

            $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
        }

        $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>');

        // Bugfix : quand les dossiers 'images' et 'commons' sont modifiés, une page est autamatiquemunt créée sans titre ce qui empèche toute modification ultérieure
        // Ce bout de script récupère la fin de l’url d’édition pour l’ajouter comme ancre
        jQuery('a.page-edit').each(function(){
            if(jQuery(this).text() == '') {
                jQuery(this).text(
                    jQuery(this).attr('href')
                        .replace('/admin/pages/','')
                        .replace('/',' ')
                        .replace(/\b\w/g, function(l){ return l.toUpperCase() })
                )
            }
        });

    }

    // Édition ou ajout d’une page
    if(location.href.indexOf('/admin/pages/') > -1 && location.href.indexOf('mode:expert') == -1 ) {
        startHelp = function(){
            introJs()
                .setOptions({
                    'prevLabel': '← Précédent',
                    'nextLabel': 'Suivant →',
                    'skipLabel': '&times;',
                    'doneLabel': '&times;',
                    'scrollTo': 'tooltip',
                    'tooltipClass':'introjs-lg',
                    'showBullets':false,
                    steps: [
                        {
                            element: '[name="data[header][title]"]',
                            intro:
                                'Saisissez le titre de votre article',
                        },
                        {
                            element: '.grav-editor-content .CodeMirror',
                            intro:
                                'La rédaction des article se fait en utilisant <a href="https://docs.framasoft.org/fr/grav/markdown.html">la syntaxe markdown</a>, mais vous avez également à disposition une barre d’outil pour vous aider. Il est aussi possible d’ajouter des modules et composants Bootstrap à l’aide de codes raccourcis. Pour en savoir plus, veuillez consulter <a href="https://docs.framasoft.org/fr/grav/">la documentation</a>.',
                            position: 'left'
                        },
                        {
                            element: '.grav-editor-button-preview',
                            intro:
                                'Pour avoir un aperçu de votre texte tel qu’il sera publié, cliquez sur ce bouton <i class="fa fa-eye"></i>. Pour revenir au mode édition, cliquez sur cette icône <i class="fa fa-code"></i>.',
                        },
                        {
                            element: 'tab-content.options.advanced1 .form-field',
                            intro:
                                'Les images associées à l’article ne peuvent être ajoutée qu’une fois l’article enregistré.<br>Sur les articles de blog, la première image sera mise à la une.<br>Pour ajouter une image dans le corps du texte, passez simplement la souris sur l’image et cliquez sur le bouton <i class="fa fa-plus-circle"></i> à sa droite.<br>Un code markdown de ce genre devrait être inséré : <code>![](une-image.jpg)</code>.<br>Les images contenues dans le dossier <code>images</code> (dossier qui se trouve dans le menu « Pages » ) s’ajoute manuellement avec ce code : <code>![](/images/une-autre-image.jpg)</code>.',
                        },
                        {
                            element: '[data-tabid="tab-content.options.advanced2"]',
                            intro:
                                'Dans les « Options » vous pouvez définir la catégorie et les mots-clé de l’article ainsi que les paramètres de publication (il est possible de la planifier à l’avance).',
                        },
                        {
                            element: '[data-tabid="tab-content.options.advanced3"]',
                            intro:
                                'L’onglet « Avancé » permet de configurer comment on accède à la page dans l’arborescence du site. S’il s’agit d’une page présente dans la barre de navigation, vous pouvez par exemple définir sa position dans le menu.',
                        },
                        {
                            element: '#titlebar button[type="submit"]',
                            intro:
                                'N’oubliez pas d’enregistrer vos changements une fois terminé.',
                        },
                    ]
                })
                .onbeforechange(function(targetElement){
                    $(targetElement).focus();
                })
                .onexit(function() {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1);
                })
                .start();

            $('.introjs-tooltiptext').before($('.introjs-tooltipbuttons'));
        }

        $('#titlebar .button-bar').prepend('<a class="button" style="background-color:#00A6CF" onclick="javascript:startHelp();"><i class="fa fa-life-ring" aria-hidden="true"></i>Aide</a>')

    }
    
    if(location.href.indexOf('/admin/tools') > -1 || location.href.indexOf('/admin/themes') > -1) {
        window.location.href = '/admin';
    }
});
